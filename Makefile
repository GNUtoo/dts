BASE ?= omap3-bug20
DTC ?= dtc
DESTDIR = bin

.PHONY: all $(DESTDIR)/$(BASE).dts $(DESTDIR)/$(BASE).dtb $(DESTDIR)/.$(BASE).dtb.dts.tmp
.PRECIOUS: $(DESTDIR)/$(BASE).dts $(DESTDIR)/$(BASE).dtb
all: $(DESTDIR)/$(BASE).dts

$(DESTDIR)/.$(BASE).dtb.dts.tmp:
	$(CC) -E -Wp,-MD,$(DESTDIR)/.$(BASE).dtb.d.pre.tmp -nostdinc -I./ -I./include -undef -D__DTS__ -x assembler-with-cpp -o $@ $(BASE).dts

$(DESTDIR)/$(BASE).dtb: $(DESTDIR)/.$(BASE).dtb.dts.tmp
	$(DTC) -O dtb -o $@ -b 0 -i ./ -i ./include/ -Wno-unit_address_vs_reg -d $(DESTDIR)/.$(BASE).dtb.d.dtc.tmp $(DESTDIR)/.$(BASE).dtb.dts.tmp

$(DESTDIR)/$(BASE).dts: $(DESTDIR)/$(BASE).dtb
	$(DTC) -O dts -o $@ -b 0 -i ./ -i ./include/ -Wno-unit_address_vs_reg -d $(DESTDIR)/.$(BASE).dtb.d.dtc.tmp $(DESTDIR)/.$(BASE).dtb.dts.tmp
