#!/bin/sh

if [ $# -ne 1 ] ; then
  echo "Usage: $0 <linux-source-directory>"
  exit 1
fi

base="$1"
ln -sf "${base}/arch/arm/boot/dts/omap3.dtsi" ./
ln -sf "${base}/arch/arm/boot/dts/omap34xx.dtsi" ./
ln -sf "${base}/arch/arm/boot/dts/omap3xxx-clocks.dtsi" ./
ln -sf "${base}/arch/arm/boot/dts/omap34xx-omap36xx-clocks.dtsi" ./
ln -sf "${base}/arch/arm/boot/dts/omap36xx-omap3430es2plus-clocks.dtsi" ./
ln -sf "${base}/arch/arm/boot/dts/omap36xx-am35xx-omap3430es2plus-clocks.dtsi" ./
ln -sf "${base}/arch/arm/boot/dts/twl4030.dtsi" ./
ln -sf "${base}/arch/arm/boot/dts/twl4030_omap3.dtsi" ./
ln -sf "${base}/include/dt-bindings" ./
